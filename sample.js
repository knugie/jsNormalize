/*jslint devel: true, browser: true, indent: 2 */

function jsNormalize(obj) {
  'use strict';
  var func,
    setter = function () {};
  for (func in obj) {
    if (obj.hasOwnProperty(func) && (typeof (obj[func]) === 'function')) {
      if ((obj[func].length === 0) && (obj[func].name === '')) {
        Object.defineProperty(obj, func, {
          get: obj[func],
          set: setter
        }
          );
      } else if ((obj[func].name !== '') && (obj[func].name[0] === obj[func].name[0].toUpperCase())) {
        obj[func] = new obj[func]();
        jsNormalize(obj[func]);
      }
    }
  }
  return obj;
}

function Main() {
  'use strict';
  var myMain = 'privateMain';
  this.yourMain = 'publicMain';
  this.foo = function () {
    console.log('Main.foo       : I am a property now. Call me but don\'t write to me.');
    return myMain + this.yourMain;
  };
  this.bar = function (a, b, c) {
    console.log('Main.bar()     : I am still a function.');
    return myMain + this.yourMain + a + b + c;
  };
  this.fuzz = function fuzz() {
    console.log('Main.fuzz()    : Call me. You know my name!');
    return myMain + this.yourMain;
  };
  this.buzz = function Buzz() {
    console.log('Main.Buzz      : I\'m out! I will never return');
    return myMain + this.yourMain;
  };
  this.Sub = function Sub() {
    var mySub = 'privateSub';
    this.yourSub = 'publicSub';
    console.log('Main.Sub       : I am a property now. Call me but don\'t write me.');
    this.foo = function () {
      console.log('Main.Sub.foo   : I am a property now. Call me but don\'t write to me.');
      return mySub + this.yourSub;
    };
    this.bar = function (a, b, c) {
      console.log('Main.Sub.bar() : Same here: I am still a function.');
      return mySub + this.yourSub + a + b + c;
    };
    this.fuzz = function fuzz() {
      console.log('Main.Sub.fuzz(): Call me. You know my name!');
      return mySub + this.yourSub;
    };
    this.buzz = function Buzz() {
      console.log('Main.Sub.Buzz  : I\'m out! I will never return');
      return mySub + this.yourSub;
    };
  };
}


var Main = jsNormalize(new Main());
console.log(Main);