jsNormalize - nicer objects for nicer code.
===========================================

The Point
---------
Which of these do you like more?

    resource.getAll()                     <= OR =>    Resource.all
    resource.getAll()[0].start()          <= OR =>    Resource.find(0).start
    form = new Form()                     <= OR =>    form = Form.create
    button = new FormButton()             <= OR =>    button = Form.Button.create

    Main                                  <= OR =>    Main
      Sub: function Sub() {                             Sub: Sub
        arguments: null                                   bar: function (a, b, c) {
        caller: null                                      buzz: Buzz
        length: 0                                         foo: —
        name: "Sub"                                       fuzz: function fuzz() {
        prototype: Sub                                    yourSub: "publicSub"
        __proto__: function Empty() {}                    __proto__: Sub
      bar: function (a, b, c) {                         bar: function (a, b, c) {
      buzz: function Buzz() {                           buzz: Buzz
      foo: function () {                                foo: —
      fuzz: function fuzz() {                           fuzz: function fuzz() {
      yourMain: "publicMain"                            yourMain: "publicMain"
      __proto__: Main                                   __proto__: Main


For some people the code on the right side just doesn't fell like Javascript (
because it doesn't say ".length" in the end ;). But that is exactly the point.
Often Javascript looks like Java.But Javascript is not Java! It's a script
language and should be as readable as possible.
(By the way: Rails is great :)

Purpose
-------
* less use of global variables
* more structured library code
* better readable production code
* simply nicer Javascript code

Features
--------
* call non-parametric methods without ()
* recursive initialization of objects

Sample
------
[https://github.com/knugie/jsNormalize/blob/master/sample.js]

Demo
----
Copy&Paste the sample.js to your (browser's) Javascript console.