/*jslint devel: true, browser: true, indent: 2 */

function jsNormalize(obj) {
  'use strict';
  var func,
    setter = function () {};
  for (func in obj) {
    if (obj.hasOwnProperty(func) && (typeof (obj[func]) === 'function')) {
      if ((obj[func].length === 0) && (obj[func].name === '')) {
        Object.defineProperty(obj, func, {
          get: obj[func],
          set: setter
        }
          );
      } else if ((obj[func].name !== '') && (obj[func].name[0] === obj[func].name[0].toUpperCase())) {
        obj[func] = new obj[func]();
        jsNormalize(obj[func]);
      }
    }
  }
  return obj;
}